package edu.udmercy.biogen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var MainActivityName = findViewById<EditText>(R.id.editTextTextPersonName)
        var btn = findViewById<Button>(R.id.button_create)

        // handle button click
        btn.setOnClickListener {
            val sendMainActivityName = MainActivityName.text.toString()

            val intent = Intent(this@MainActivity, ResultsActivity::class.java)
            intent.putExtra("MainActivityName", sendMainActivityName)
            startActivity(intent)
        }
    }
}