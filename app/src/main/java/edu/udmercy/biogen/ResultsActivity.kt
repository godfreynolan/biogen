package edu.udmercy.biogen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import org.w3c.dom.Text

class ResultsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)

        // get data from intent
        val intent = intent
        val ResultsName = intent.getStringExtra("MainActivityName")


        // find the textView
        val bio = findViewById<TextView>(R.id.textView)

        // set the text to the captured name
        bio.text = ResultsName

    }
}